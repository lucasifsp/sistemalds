<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('upload', 'ProjetoController@upload')->name('files.upload');
Route::get('usuario/{projetoId}/download/{fileId}', 'ProjetoController@download')->name('files.download');
Route::get('usuario/{projetoId}/remover/{fileId}', 'ProjetoController@destroyFile')->name('files.destroy');

Route::prefix('/admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@index')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
});

Route::prefix('/projetos')->group(function() {
    Route::get('/', 'ProjetoController@index')->name('projetos');
    Route::get('/novo', 'ProjetoController@create')->name('criarprojeto');
    Route::post('/', 'ProjetoController@store');
    Route::get('/apagar/{id}', 'ProjetoController@destroy');
    Route::get('/editar/{id}', 'ProjetoController@edit');
    Route::post('/{id}', 'ProjetoController@update');
    Route::post('/aluno/{id}', 'ProjetoController@updateUser');
    Route::get('/detalhes/{id}', 'ProjetoController@show');
    Route::post('/comentario/{id}', 'ProjetoController@addComentario');
    Route::post('/file/{id}', 'ProjetoController@addFile');
});








