@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card border-dark mb-3 text-white  bg-secondary mb-3">
                <div class="card-header bg-dark"><h3>Dashboard - Aluno</h3></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3>Você está logado como Aluno</h3>
                        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

