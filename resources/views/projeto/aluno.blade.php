@extends('layouts.projetoaluno')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card border-dark mb-3 text-white  bg-secondary mb-3">
                <div class="card-header bg-dark"> <h3>Dashboard - {{$projeto->title}}</h3></div>

                <div class="card-body ">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h5 class="card-title">Informações do projeto</h5>

                    <table class="table table-ordered table-hover table-bordered table-dark">
                        <thead >
                            <tr>
                                <th>Código</th>
                                <th>Título</th>
                                <th>Notas</th>
                                <th>Professor</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$projeto->id}}</td>
                                <td>{{$projeto->title}}</td>
                                <td>{{$projeto->notes}}</td>
                                <td>{{$professor[0]['name']}}</td>
                            </tr>       
                        </tbody>
                    </table>

                    <!-- Alunos -->
                    <h5 class="card-title">Informações dos alunos</h5>
            @if(count($alunos) > 0)
                    <table class="table table-ordered table-hover table-bordered table-dark">
                        <thead > 
                            <tr>
                                <th>Prontuário</th>
                                <th>Nome</th>
                                <th>E-mail</th>
                                <th>Formação</th>
                            </tr>
                        </thead>
                        <tbody>
                @foreach($alunos as $aluno)
                            <tr>
                                <td>{{$aluno->registration}}</td>
                                <td>{{$aluno->name}}</td>
                                <td>{{$aluno->email}}</td>
                                <td>{{$aluno->formation}}</td>
                            </tr>
                @endforeach                
                        </tbody>
                    </table>
            @endif

                    <!-- Comentários -->
                    <!--<h5 class="card-title">Comentários</h5>
            @if(count($comentarios) > 0)
                    <table class="table table-ordered table-hover">
                        <thead>
                            <tr>
                                <th>Comentário</th>
                                <th>Enviado em</th>
                            </tr>
                        </thead>
                        <tbody>
                @foreach($comentarios as $comentario)
                            <tr>
                                <td>{{$comentario->comentario}}</td>
                                <td>{{$comentario->created_at}}</td>
                            </tr>
                @endforeach                
                        </tbody>
                    </table>
            @endif-->

                    <!-- Upload -->
                    <h5>Enviar arquivo:</h5>
                    <span class="btn btn-danger fileinput-button">
                    <i class="material-icons">attach_file</i>
                        <span class="vertical-align: baseline"><b>Selecionar arquivo</b></span>
                        <input id="fileupload" type="file" name="documento"
                        data-token="{{ csrf_token() }}"
                        data-projeto-id="{{ $projeto->id }}">
                    </span>

                    <hr/>

                    <!--<table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Enviado em</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($files as $file)
                            <tr>
                                <td>{!! $file->name !!}</td>
                                <td>{!! $file->created_at !!}</td>
                                <td>
                                    <a href="{!! route('files.download', [$projeto->id, $file->id]) !!}" class="btn btn-sm btn-success">Download</a>
                                    <a href="{!! route('files.destroy', [$projeto->id, $file->id]) !!}" class="btn btn-sm btn-danger">Excluir</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>-->

                    <!-- Comentários e Arquivos -->
                    <h5 class="card-title">Comentários e Arquivos</h5>
            @if(count($comentarios) > 0 || count($files))
                    <table class="table table-ordered table-hover table-bordered table-dark">
                        <thead>
                @foreach($FilesComentarios as $elemento)
                    @if(isset($elemento->comentario))
                            <tr>
                                <th>Comentário</th>
                                <th>Enviado em</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$elemento->comentario}}</td>
                                <td>{{$elemento->created_at}}</td>
                                <td></td>
                            </tr>
                    @endif
                    @if(isset($elemento->name))
                            <tr>
                                <th>Arquivo</th>
                                <th>Enviado em</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{!! $elemento->name !!}</td>
                                <td>{!! $elemento->created_at !!}</td>
                                <td>
                                    <a href="{!! route('files.download', [$projeto->id, $elemento->id]) !!}" class="btn btn-sm btn-success">Download</a>
                                    <a href="{!! route('files.destroy', [$projeto->id, $elemento->id]) !!}" class="btn btn-sm btn-danger">Excluir</a>
                                </td>
                            </tr>
                    @endif
                @endforeach                
                        </tbody>
                    </table>
            @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
@parent
<script>
    ;(function($){
        'use strict';
        $(document).ready(function(){
            var $fileupload = $('#fileupload'),
	  		$upload_success = $('#upload-success');

            $fileupload.fileupload({
                url: '/upload',
                formData: {_token: $fileupload.data('token'), projetoId: $fileupload.data('projetoId')},
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                },
                done: function (e, data) {
                    $upload_success.removeClass('hide').hide().slideDown('fast');

                    setTimeout(function(){
                        location.reload();
                    }, 2000);
                }
            });
        });
    })(window.jQuery);
</script>
@stop