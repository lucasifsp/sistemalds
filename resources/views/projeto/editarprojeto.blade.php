@extends('layouts.projeto')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card border-dark mb-3 text-white  bg-secondary mb-3">
                    <div class="card-header bg-dark">Dashboard - Editar Projeto</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="/projetos/{{$proj->id}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="title">Título do Projeto</label>
                                <input type="text" class="form-control" name="title" value="{{$proj->title}}"
                                        id="title" placeholder="Título do Projeto">
                            </div>
                            <div class="form-group">
                                <label for="notaProjeto">Notas</label>
                                <input type="text" class="form-control" name="notaProjeto" value="{{$proj->notes}}"
                                        id="notaProjeto" placeholder="Nota">
                            </div>
                            <div class="form-group">
                                <label for="ProfessorProjeto">Professor</label>
                                <select type="text" class="form-control" name="ProfessorProjeto"
                                        id="ProfessorProjeto" placeholder="Professor" selected="{{$prof}}">
                @foreach($professores as $professor)
                                    <option value="{{$professor->id}}">{{$professor->name}}</option>
                @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm"><i class="material-icons">save</i><b> Salvar</b></button>
                            <button type="cancel" class="btn btn-danger btn-sm"><i class="material-icons">cancel</i><b> Cancelar</b></button>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection