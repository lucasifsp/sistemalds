@extends('layouts.projeto')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card border-dark mb-3 text-white  bg-secondary mb-3">
                <div class="card-header bg-dark"><h3>Dashboard - Projetos</h3></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h5 class="card-title">Projetos</h5>

@if(count($projetos_user) > 0)
        <table class="table table-ordered table-hover table-bordered table-dark">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Título</th>
                    <th>Notas</th>
                    <th>Professor</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
    @foreach($projetos_user as $proj)
                <tr>
                    <td>{{$proj->id}}</td>
                    <td>{{$proj->title}}</td>
                    <td>{{$proj->notes}}</td>
                    <td>{{$user->name}}</td>
                    <td>
                        <a href="/projetos/detalhes/{{$proj->id}}" class="btn btn-sm btn-primary vertical-align: baseline"><i class="material-icons">dehaze</i><b> Detalhes</b></a>
                        <a href="/projetos/editar/{{$proj->id}}" class="btn btn-sm btn-primary"><i class="material-icons">create</i><b> Editar</b></a>
                        <a href="/projetos/apagar/{{$proj->id}}" class="btn btn-sm btn-danger"><i class="material-icons">delete</i><b> Apagar </b></a>
                    </td>
                </tr>
    @endforeach                
            </tbody>
        </table>
@endif
                    
                </div>

                
                <div class="card-footer">
                    <a href="{{route('criarprojeto')}}" class="btn btn-sm btn-primary" role="button"><i class="material-icons">add</i><b> Novo projeto</b></a>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection