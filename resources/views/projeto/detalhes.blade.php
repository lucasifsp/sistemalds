@extends('layouts.projeto')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card border-dark mb-3 text-white  bg-secondary mb-3">
                <div class="card-header bg-dark">Dashboard - {{$projeto->title}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h5 class="card-title">Informações do projeto</h5>

                    <table class="table table-ordered table-hover table-bordered table-dark">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Título</th>
                                <th>Notas</th>
                                <th>Professor</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$projeto->id}}</td>
                                <td>{{$projeto->title}}</td>
                                <td>{{$projeto->notes}}</td>
                                <td>{{$user->name}}</td>
                            </tr>       
                        </tbody>
                    </table>

                    <!-- Alunos -->
                    <h5 class="card-title">Informações dos alunos</h5>
            @if(count($alunos) > 0)
                    <table class="table table-ordered table-hover table-bordered table-dark">
                        <thead>
                            <tr>
                                <th>Prontuário</th>
                                <th>Nome</th>
                                <th>E-mail</th>
                                <th>Formação</th>
                            </tr>
                        </thead>
                        <tbody>
                @foreach($alunos as $aluno)
                            <tr>
                                <td>{{$aluno->registration}}</td>
                                <td>{{$aluno->name}}</td>
                                <td>{{$aluno->email}}</td>
                                <td>{{$aluno->formation}}</td>
                            </tr>
                @endforeach                
                        </tbody>
                    </table>
            @endif

            <form action="/projetos/aluno/{{$projeto->id}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="AlunoProjeto">Adicionar aluno</label>
                    <select type="text" class="form-control" name="AlunoProjeto"
                            id="AlunoProjeto" placeholder="Aluno">
                    <option value="" selected>Selecione um aluno</option>
            @foreach($todos_alunos as $aluno)
                        <option value="{{$aluno->id}}">{{$aluno->name}}</option>
            @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-sm"><i class="material-icons">person_add</i> Adicionar aluno</button>
            </form>

            <hr/>

                    <!-- Comentários -->
                    <!--<h5 class="card-title">Comentários</h5>
            @if(count($comentarios) > 0)
                    <table class="table table-ordered table-hover">
                        <thead>
                            <tr>
                                <th>Comentário</th>
                                <th>Enviado em</th>
                            </tr>
                        </thead>
                        <tbody>
                @foreach($comentarios as $comentario)
                            <tr>
                                <td>{{$comentario->comentario}}</td>
                                <td>{{$comentario->created_at}}</td>
                            </tr>
                @endforeach                
                        </tbody>
                    </table>
            @endif-->

                    <br/>

                    <form action="/projetos/comentario/{{$projeto->id}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="ComentarioProjeto">Adicionar comentário</label>
                            <textarea class="form-control" id="comentario" name="comentario"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm"><i class="material-icons">comment</i> Adicionar comentário</button>
                    </form>

                    <hr/>

                    <form action="/projetos/file/{{$projeto->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <span>Selecionar arquivo...</span>
                            <input class="btn" id="file" type="file" name="file">
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm"><i class="material-icons">attach_file</i> Adicionar arquivo</button>
                    </form>

                    <hr/>

                    <!--<table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Enviado em</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($files as $file)
                            <tr>
                                <td>{!! $file->name !!}</td>
                                <td>{!! $file->created_at !!}</td>
                                <td>
                                    <a href="{!! route('files.download', [$projeto->id, $file->id]) !!}" class="btn btn-sm btn-success">Download</a>
                                    <a href="{!! route('files.destroy', [$projeto->id, $file->id]) !!}" class="btn btn-sm btn-danger">Excluir</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>-->

                    <!-- Comentários e Arquivos -->
                    <h5 class="card-title">Comentários e Arquivos</h5>
            @if(count($comentarios) > 0 || count($files))
                    <table class="table table-ordered table-hover table-bordered table-dark">
                        <thead>
                @foreach($FilesComentarios as $elemento)
                    @if(isset($elemento->comentario))
                            <tr>
                                <th>Comentário</th>
                                <th>Enviado em</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$elemento->comentario}}</td>
                                <td>{{$elemento->created_at}}</td>
                                <td></td>
                            </tr>
                    @endif
                    @if(isset($elemento->name))
                            <tr>
                                <th>Arquivo</th>
                                <th>Enviado em</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{!! $elemento->name !!}</td>
                                <td>{!! $elemento->created_at !!}</td>
                                <td>
                                    <a href="{!! route('files.download', [$projeto->id, $elemento->id]) !!}" class="btn btn-sm btn-success">Download</a>
                                    <a href="{!! route('files.destroy', [$projeto->id, $elemento->id]) !!}" class="btn btn-sm btn-danger">Excluir</a>
                                </td>
                            </tr>
                    @endif
                @endforeach                
                        </tbody>
                    </table>
            @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection