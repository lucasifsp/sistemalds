<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projaluno extends Model
{
    protected $fillable = [
        'user_id','projeto_id'
    ];
}
