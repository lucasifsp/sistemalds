<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projeto extends Model
{
    protected $fillable = [
        'title', 'notes', 'user_id'
    ];

    /**
	* Relationships
	*/
	public function files()
	{
		return $this->hasMany('App\File');
	}

	/**
	* Relationships
	*/
	public function comentarios()
	{
		return $this->hasMany('App\Comentario');
	}
}
