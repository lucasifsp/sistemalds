<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Find;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipo = auth()->user()->type;
        
        if($tipo == "aluno"){

            return view('home');
        }
        if($tipo == "professor"){

            return view('homeprofessor');
        }
    }
}
