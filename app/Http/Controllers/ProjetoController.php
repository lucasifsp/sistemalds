<?php

namespace App\Http\Controllers;

use App\Projeto;
use App\User;
use App\Projaluno;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjetoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tipo = auth()->user()->type;

        if($tipo == "aluno"){
            $projeto_id = auth()->user()->projeto_id;

            if(isset($projeto_id)){
                $projeto = Projeto::with('files')->find($projeto_id);
                $files = DB::table('files')->where('projeto_id',$projeto_id)->get();
                $professor = User::where('id',$projeto->user_id)->get();
                $professor = $professor->toArray();
                $alunos = User::select(['id','registration','name','email','formation'])->where('type','aluno')->where('projeto_id',$projeto_id)->get();
                $comentarios = DB::table('comentarios')->where('projeto_id',$projeto_id)->get();
                
                /*$arrFiles = $files->toArray();
                $arrComentarios = $comentarios->toArray();
                
                $arrFilesComentarios = array_merge($arrFiles,$arrComentarios);*/

                //$arrayOrdenado = collect($arrFilesComentarios)->sortBy('created_at')->reverse()->toArray();

                $FilesComentarios = $files->merge($comentarios)->sortBy('created_at')->reverse();

                //dd($mergedOrdenado);
                
                return view('projeto.aluno', compact('professor','projeto','alunos','files','comentarios','FilesComentarios'));
            }
            
            return view('home');
            
        }
        if($tipo == "professor"){
            $user_id = auth()->user()->id;
            $projetos_user = Projeto::select(['id','title','notes'])->where('user_id',$user_id)->get();
            $user = User::find($user_id);

            return view('projeto.projetos', compact('projetos_user','user'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipo = auth()->user()->type;
        $professores = User::select(['name','id'])->where('type','professor')->get();
        
        if($tipo == "aluno"){
            return back();
        }
        if($tipo == "professor"){
            return view('projeto.novoprojeto', compact('professores'));
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proj = new Projeto();
        $proj->title = $request->input('title');
        $proj->notes = $request->input('notaProjeto');
        $proj->user_id = $request->input('ProfessorProjeto');
        $proj->save();
        return redirect('/projetos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Projeto  $projeto
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $projeto = Projeto::with('files')->find($id);
        $files = DB::table('files')->where('projeto_id',$id)->get();
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $alunos = User::select(['id','registration','name','email','formation'])->where('type','aluno')->where('projeto_id',$id)->get();
        $todos_alunos = User::select(['name','id'])->where('projeto_id','<>',$id)->orWhereNull('projeto_id')->
                where('type','aluno')->get();
        $comentarios = DB::table('comentarios')->where('projeto_id',$id)->get();

        $FilesComentarios = $files->merge($comentarios)->sortBy('created_at')->reverse();

        if(isset($projeto)){
            return view('projeto.detalhes',compact('projeto','alunos','todos_alunos','user','files','comentarios','FilesComentarios'));
        }
        return redirect('/projetos');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Projeto  $projeto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proj = Projeto::find($id);
        //$prof = User::select('name')->where('id',$proj->user_id)->get();
        $prof_id = auth()->user()->id;
        $prof = User::find($prof_id);
        $professores = User::select(['name','id'])->where('type','professor')->get();
        if(isset($proj)) {
            return view('projeto.editarprojeto', compact(['proj','prof','professores']));
        }
        return redirect('/projetos');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Projeto  $projeto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proj = Projeto::find($id);
        if(isset($proj)) {
            $proj->title = $request->input('title');
            $proj->notes = $request->input('notaProjeto');
            $proj->user_id = $request->input('ProfessorProjeto');
            $proj->save();
        }
        return redirect('/projetos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Projeto  $projeto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proj = Projeto::find($id);
        if (isset($proj)) {
            $proj->delete();
        }
        return redirect('/projetos');
    }

    public function updateUser(Request $request, $id){
        $proj = Projeto::find($id);
        $id_aluno = $request->input('AlunoProjeto');
        $user = User::find($id_aluno);
        if(isset($user)) {
            $user->projeto_id = $proj->id;
            $user->update();
        }
        
        return redirect('/projetos');
    }

    public function upload()
	{
		/**
		* Request related
		*/
		$file = \Request::file('documento');

		$projetoId = \Request::get('projetoId');

		/**
		* Storage related
		*/
		$storagePath = storage_path().'/documentos/'.$projetoId;

		$fileName = $file->getClientOriginalName();

		/**
		* Database related
		*/
		$fileModel = new \App\File();
		$fileModel->name = $fileName;

		$projeto = \App\Projeto::find($projetoId);

		$projeto->files()->save($fileModel);

		return $file->move($storagePath, $fileName);
	}

	public function download($projetoId, $fileId)
	{
		$file = \App\File::find($fileId);

		$storagePath = storage_path().'/documentos/'.$projetoId;

		return \Response::download($storagePath.'/'.$file->name);
	}

	public function destroyFile($projetoId, $fileId)
	{
		$file = \App\File::find($fileId);

		$storagePath = storage_path().'/documentos/'.$projetoId;

		$file->delete();

		unlink($storagePath.'/'.$file->name);

		return redirect()->back()->with('success', 'Arquivo removido com sucesso!');
    }
    
    public function addComentario(Request $request, $id){
        $proj = Projeto::find($id);
        $comentario = $request->input('comentario');
        $comentarioModel = new \App\Comentario();
        $comentarioModel->comentario = $comentario;
        $proj->comentarios()->save($comentarioModel);
        return redirect()->back()->with('Comentário adicionado com sucesso!');
    }

    public function addFile(Request $request, $id){

        $file = $request->file('file');

        $storagePath = storage_path().'/documentos/'.$id;

        $fileName = $file->getClientOriginalName();

        $fileModel = new \App\File();
        $fileModel->name = $fileName;

        $proj = Projeto::find($id);
        $proj->files()->save($fileModel);

        $file->move($storagePath, $fileName);

        return redirect()->back()->with('Arquivo adicionado com sucesso!');
		
    }
}
