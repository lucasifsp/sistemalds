<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    /**
    * Relationships
    */
    public function projeto()
    {
        return $this->belongsTo('App\Projeto', 'projeto_id');
    }
}
