<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjalunosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projalunos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned(); //id aluno
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('projeto_id')->unsigned(); //id projeto
            $table->foreign('projeto_id')->references('id')->on('projetos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projalunos');
    }
}
